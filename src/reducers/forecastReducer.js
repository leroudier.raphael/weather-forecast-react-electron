let initialState = {
    weatherForecast: {
        city:null,
        localtime:null,
        weather_description:null,
        temperature:null,
        weather_icon:null,
        wind_speed:null,
        humidity:null
    },
    loader: false,
    city: 'Lyon',
}

export const forecastReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_FORECAST':
            return {
                ...state,
                weatherForecast:action.weatherForecast
            };
            //console.log(weatherForecast)
        case 'CHOSEN_CITY':
            return {
                ...state,
                city: action.value
            };
        default:
            return state;
    }
};

export default forecastReducer;