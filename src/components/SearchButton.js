import React from 'react';

class SearchButton extends React.Component{
    render(){
    return (
        <div className="search-button">
            <button onClick={this.props.onClick}>Choose you city</button>
        </div>
    );
}
}

export default SearchButton;
