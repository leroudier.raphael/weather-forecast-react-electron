import React from 'react';
import { connect } from 'react-redux';


class WeatherResult extends React.Component {
    render() {
        return (
            <div className="weather-result">
                <div className="main-info">
                        <img src={this.props.weatherForecast.weather_icon}></img>
                        <p>{this.props.weatherForecast.weather_description}</p>
                        <p>{this.props.weatherForecast.temperature}</p>
                </div>
                <div className="additionnal-info">
                    <p>{this.props.weatherForecast.wind_speed}</p>
                    <p>{this.props.weatherForecast.humidity}</p>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        weatherForecast: state.forecastReducer.weatherForecast,
    }
}


export default connect(mapStateToProps)(WeatherResult)

